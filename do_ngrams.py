import psycopg2, sys, requests, logging
from google_ngram_downloader import readline_google_store

logging.basicConfig(level=logging.DEBUG)

try:
   conn = psycopg2.connect("dbname='ngrams' user='postgres' host='localhost'");
except:
   print "I am unable to connect to the database"
   sys.exit()

with open("google_suffixes.txt") as f: google_suffixes = f.read().splitlines()

vocabulary = []
with open("vocabulary.txt") as f:
    for row in f.read().splitlines():
        words = eval(row)
        vocabulary += [word.decode('utf8').lower() for word in words]


def parse_word(word):
    if any([suffix in word for suffix in google_suffixes]):
        for suffix in google_suffixes:
            word = word.replace(suffix, "")
    return word.lower()

def parse_ngram(words):
    return [parse_word(word) for word in words.split(" ")]

def do_ngrams(indices):
    cur = conn.cursor()
    for fname, url, records in readline_google_store(ngram_len=5, indices=indices, verbose=True):
        for record in records:
            parsed_ngram = parse_ngram(record.ngram)
            if not all([word in vocabulary for word in parsed_ngram]):
                print("SKIP %s %d %d %d" % (str(parsed_ngram), record.year, record.match_count, record.volume_count))
                continue
            else:
                wordz = [vocabulary.index(word) for word in parsed_ngram]
                try:
                    cur.execute("INSERT INTO ngrams (n, word1, word2, word3, word4, word5, year, total_count, volume_count) VALUES (%d, %s, %s, %s, %s, %s, %d, %d, %d);" % (5, wordz[0], wordz[1], wordz[2], wordz[3], wordz[4], record.year, record.match_count, record.volume_count))
                    print("GOOD %s %d %d %d" % (str(wordz), record.year, record.match_count, record.volume_count))
                except psycopg2.IntegrityError:
                    print("ERROR %s %d %d %d" % (str(wordz), record.year, record.match_count, record.volume_count))
                    conn.rollback()
                    continue
                else:
                    conn.commit()
    return True