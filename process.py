import psycopg2, sys, requests, logging, argparse
from google_ngram_downloader.util import get_indices

logging.basicConfig(level=logging.DEBUG)

from do_ngrams import do_ngrams, vocabulary

try:
   conn = psycopg2.connect("dbname='ngrams' user='postgres' host='localhost'");
except:
   print "I am unable to connect to the database"
   sys.exit()

parser = argparse.ArgumentParser()
parser.add_argument("command")
args = parser.parse_args()

if __name__ == "__main__":
    if args.command == "vocabulary":
        cur = conn.cursor()
        for i, word in enumerate(vocabulary):
            print word
            word = word.replace("'", "''").lower()
            cur.execute("SELECT id, word FROM words WHERE word = \'%s\'" % word)
            if not cur.fetchone(): cur.execute("INSERT INTO words VALUES (%d, \'%s\');" % (i, word))
        conn.commit()
    elif args.command == "ngrams":
        from redis import Redis
        from rq import Queue
        q = Queue(connection=Redis())
        for index in list(get_indices(5)): q.enqueue_call(func=do_ngrams, args=([index],), timeout=3600)
        print "added %d jobs to the queue" % len(list(get_indices(5)))
    else:
        print "command not recognized"
