from __future__ import division
import psycopg2, sys, math


try:
   conn = psycopg2.connect("dbname='ngrams' user='postgres' host='localhost'");
except:
   print "I am unable to connect to the database"
   sys.exit()

cur = conn.cursor()

with open("find_quadgram_context_with_fifth_word.sql") as f: query1 = f.read()
cur.execute(query1)
quadgrams = [row for row in cur.fetchall()]

with open("find_total_count_for_pentagram.sql") as f: query2 = f.read()
cur.execute(query2)
pentagrams = [row for row in cur.fetchall()]
pentagrams = {(x[0], x[1], x[2], x[3], x[4]):(x[5],x[6]) for x in pentagrams}

vocabulary = []
with open("vocabulary.txt") as f:
    for row in f.read().splitlines():
        words = eval(row)
        vocabulary += [word.decode('utf8') for word in words]

# for pentagram in pentagrams:
# 	print pentagram
contextFile = open('contexts/contextFile0.txt', 'w+')
contextsAndWeightedWords = {}
contextCount = 0
for word1, word2, word3, word4, potential_words5, num_potential_word5 in quadgrams:
	total_count = 0
	contextsAndWeightedWords[(word1, word2, word3, word4)] = {}
	if num_potential_word5 > 2:
		for potential_word in potential_words5:
			match_count, volume_count = pentagrams[(word1, word2, word3, word4, potential_word)]
			contextsAndWeightedWords[(word1, word2, word3, word4)][potential_word] = match_count
			total_count += match_count
		contextFile.write("Context Words: %s %s %s %s \n" % (word1, word2, word3, word4))
		contextCount += 1
		#print("Context Words: %s %s %s %s \n" % (word1, word2, word3, word4))
		#print("total count is %d \n" % (total_count))
		for potential_word in contextsAndWeightedWords[(word1, word2, word3, word4)]:
			wordProbability = contextsAndWeightedWords[(word1, word2, word3, word4)][potential_word] / total_count
			#print(type(contextsAndWeightedWords[(word1, word2, word3, word4)][potential_word]))
			for vocabWord in vocabulary:
				if potential_word.lower() == vocabWord:
					potential_word = vocabWord
					break;
				elif potential_word == vocabWord.lower():
					potential_word = vocabWord
			contextFile.write("%s %f \n" % (potential_word, wordProbability))
		if(contextCount % 1000 == 0):
			fileNumber = contextCount / 1000
			contextFile.close()
			print('Starting file contextFile%d.txt' % (fileNumber))
			contextFile = open('contexts/contextFile%d.txt' % (fileNumber), 'w+')

contextFile.close()
		# print word1, word2, word3, word4, potential_word, match_count, volume_count