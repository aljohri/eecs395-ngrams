-- dropdb ngrams && createdb ngrams
-- cat createdb.sql | psql ngrams

CREATE table words (
	id BIGSERIAL PRIMARY KEY,
	word varchar,
	constraint word_the_word UNIQUE (word)
);

-- max ngram = 5
CREATE table ngrams (
	id BIGSERIAL  PRIMARY KEY,
	n INTEGER NOT NULL,
	word1 INTEGER NOT NULL REFERENCES words(id),
	word2 INTEGER NOT NULL REFERENCES words(id),
	word3 INTEGER NOT NULL REFERENCES words(id),
	word4 INTEGER NOT NULL REFERENCES words(id),
	word5 INTEGER NOT NULL REFERENCES words(id),
	year INTEGER NOT NULL,
	total_count INTEGER NOT NULL,
	volume_count INTEGER NOT NULL,
	constraint pentagram UNIQUE (word1, word2, word3, word4, word5, year)
);

CREATE INDEX ON ngrams (word1, word2, word3, word4);