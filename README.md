## Backup Database

```
pg_dump -Fc --schema=public --no-owner ngrams > ngrams.compressed.psql
```